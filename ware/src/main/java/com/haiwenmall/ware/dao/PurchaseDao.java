package com.haiwenmall.ware.dao;

import com.haiwenmall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:43:15
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {

}
