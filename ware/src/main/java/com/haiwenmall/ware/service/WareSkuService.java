package com.haiwenmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.to.OrderTo;
import com.haiwenmail.common.to.SkuStockTo;
import com.haiwenmail.common.to.mq.StockLockedTo;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.ware.entity.WareSkuEntity;
import com.haiwenmall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:43:15
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuStockTo> getSkuHasStock(List<Long> skuIds);

    boolean orderLockStock(WareSkuLockVo vo);

    void unlockStock(StockLockedTo to);

    void unlockStock(OrderTo to);
}

