package com.haiwenmall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.haiwenmail.common.utils.R;
import com.haiwenmall.ware.feign.MemberFeignService;
import com.haiwenmall.ware.vo.FareVo;
import com.haiwenmall.ware.vo.MemberAddressVo;
import com.haiwenmall.ware.dao.WareInfoDao;
import com.haiwenmall.ware.entity.WareInfoEntity;
import com.haiwenmall.ware.service.WareInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmail.common.utils.Query;

import org.springframework.util.StringUtils;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Autowired
    private MemberFeignService memberFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareInfoEntity> queryWrapper = new QueryWrapper<>();
        String key = (String)params.get("key");
        if (!StringUtils.isEmpty(key)){
            queryWrapper.eq("id",key).or().like("name",key)
                    .or().like("areacode",key)
                    .or().like("address",key);
        }
        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                queryWrapper
        );
        return new PageUtils(page);
    }

    @Override
    public FareVo getFare(Long addrId) {
        FareVo fareVo = new FareVo();
        R r = memberFeignService.addrInfo(addrId);
        MemberAddressVo data = r.getData("memberReceiveAddress",new TypeReference<MemberAddressVo>(){});
        if (data != null){
            //TODO 运费，暂不计算，先取10元
            BigDecimal fare = new BigDecimal(10);
            fareVo.setAddress(data);
            fareVo.setFare(fare);
            return fareVo;
        }
        return null;
    }

}