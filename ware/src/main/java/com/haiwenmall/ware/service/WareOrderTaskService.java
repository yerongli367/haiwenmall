package com.haiwenmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:43:15
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);

    WareOrderTaskEntity getOrderTaskByOrderSn(String orderSn);
}

