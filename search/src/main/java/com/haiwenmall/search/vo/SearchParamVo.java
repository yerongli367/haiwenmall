package com.haiwenmall.search.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParamVo {
    /**
     * 全文检索(模糊查询)
     */
    private String keyword;//页面传过来的全文匹配关键字
    /**
     * 排序条件
     *
     * saleCount_asc/desc
     * skuPeice_asc/desc
     * hotScore_asc/desc
     */
    private String soft;
    /**
     * 过滤条件
     *
     * 三级分类id
     * 是否有货
     * 价格区间
     * 品牌id（可多选 or）
     * 属性（可多选 相同属性or 不同属性and）
     * 页码
     */
    private Long catalog3Id;
    private Integer hasStock;
    private String skuPrice;
    private List<Long> brandId;
    private List<String> attrs;
    private Integer pageNum = 1;
    /**
     * 原生的所有查询条件
     */
    private String _queryString;
}
