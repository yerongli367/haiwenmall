package com.haiwenmall.search.controller;

import com.haiwenmall.search.service.MallSearchService;
import com.haiwenmall.search.vo.SearchParamVo;
import com.haiwenmall.search.vo.SearchResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SearchController {

    @Autowired
    private MallSearchService mallSearchService;


    /**
     *springmvc自动将地址栏中的参数封装成指定成的对象
     */
    @GetMapping("/list.html")
    public String listPage(SearchParamVo param, Model model, HttpServletRequest request){

        param.set_queryString(request.getQueryString());
        SearchResponseVo result = mallSearchService.search(param);
        model.addAttribute("result",result);
        return "list";
    }
}
