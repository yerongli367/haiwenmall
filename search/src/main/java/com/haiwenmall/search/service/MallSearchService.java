package com.haiwenmall.search.service;

import com.haiwenmall.search.vo.SearchParamVo;
import com.haiwenmall.search.vo.SearchResponseVo;

public interface MallSearchService {
    SearchResponseVo search(SearchParamVo param);
}
