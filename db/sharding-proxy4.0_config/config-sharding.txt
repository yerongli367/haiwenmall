######################################################################################################
#
# If you want to connect to MySQL, you should manually copy MySQL driver to lib directory.
#
######################################################################################################
【本地的mysql模拟两个库】
schemaName: sharding_db_cluster
#配置数据源
dataSources:
  ds_0: #第1个主mysql，连接 oms库
    url: jdbc:mysql://127.0.0.1:3306/oms?serverTimezone=UTC&useSSL=false
    username: root
    password: 23519290
    connectionTimeoutMilliseconds: 30000
    idleTimeoutMilliseconds: 60000
    maxLifetimeMilliseconds: 1800000
    maxPoolSize: 50 
  ds_1: #第2个主mysql连接 gulimall_oms_2 库
    url: jdbc:mysql://127.0.0.1:3306/oms_2?serverTimezone=UTC&useSSL=false
    username: root
    password: 23519290
    connectionTimeoutMilliseconds: 30000
    idleTimeoutMilliseconds: 60000
    maxLifetimeMilliseconds: 1800000
    maxPoolSize: 50
#==============================================

#定义规则
shardingRule:
  tables: 
    #订单表
    t_order: 
      # ds_${0..1}和上面配置的数据源（ds_0,ds_1）对应。表划分成t_order_1、t_order_2 ,一个库里面2张表
      actualDataNodes: ds_${0..1}.t_order_${0..1}
      # 分表策略
      tableStrategy:
        inline:
          shardingColumn: order_id  #用order_id来分表
          algorithmExpression: t_order_${order_id % 2} # 算法->求余
      # order_id生成的算法，用雪花算法
      keyGenerator:
        type: SNOWFLAKE
        column: order_id
        
#-------------------------     
    #订单项表
    t_order_item:
      #分成2个库，2张表
      actualDataNodes: ds_${0..1}.t_order_item_${0..1}
      #指定分表策略
      tableStrategy:
        inline:
          #用order_id来分
          shardingColumn: order_id
          #算法-->求余
          algorithmExpression: t_order_item_${order_id % 2}
      # order_item_id 自动生成用雪花算法
      keyGenerator:
        type: SNOWFLAKE
        column: order_item_id
#-------------------------  

  #绑定表(表的关系)-好处：省得跨库找，不会产生笛卡儿积，速度快，效率高
  bindingTables:
    - t_order,t_order_item
#-------------------------

   
  #数据库策略-分库策略====>流程：先用user_id进行了分库。然后在根据order_id来分表
  defaultDatabaseStrategy:
    inline:
      # 用user_id 列来分
      shardingColumn: user_id
      # 算法 -->求余数
      algorithmExpression: ds_${user_id % 2}
      
  #默认的分表策略。共用部分
  defaultTableStrategy:
    none: