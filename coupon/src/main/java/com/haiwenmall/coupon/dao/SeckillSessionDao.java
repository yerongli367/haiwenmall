package com.haiwenmall.coupon.dao;

import com.haiwenmall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:35:18
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {

}
