package com.haiwenmall.coupon.dao;

import com.haiwenmall.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:35:17
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {

}
