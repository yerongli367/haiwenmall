package com.haiwenmall.coupon.dao;

import com.haiwenmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:35:17
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {

}
