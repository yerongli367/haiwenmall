package com.haiwenmall.coupon.dao;

import com.haiwenmall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2022-03-17 16:12:36
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
