package com.haiwenmall.coupon.service.impl;

import com.haiwenmall.coupon.entity.SeckillSkuRelationEntity;
import com.haiwenmall.coupon.service.SeckillSkuRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmail.common.utils.Query;

import com.haiwenmall.coupon.dao.SeckillSessionDao;
import com.haiwenmall.coupon.entity.SeckillSessionEntity;
import com.haiwenmall.coupon.service.SeckillSessionService;
import org.springframework.util.CollectionUtils;


@Service("seckillSessionService")
public class SeckillSessionServiceImpl extends ServiceImpl<SeckillSessionDao, SeckillSessionEntity> implements SeckillSessionService {

    @Autowired
    private SeckillSkuRelationService seckillSkuRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillSessionEntity> page = this.page(
                new Query<SeckillSessionEntity>().getPage(params),
                new QueryWrapper<SeckillSessionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SeckillSessionEntity> getLate3DaySession() {
        //计算最近三天的时间
        List<SeckillSessionEntity> sessionEntities = this.list(new QueryWrapper<SeckillSessionEntity>().between("start_time",starTime(),endTime()));
        if (!CollectionUtils.isEmpty(sessionEntities)){
            list().stream().map(sessionEntity -> {
                Long sessionId = sessionEntity.getId();
                List<SeckillSkuRelationEntity> list = seckillSkuRelationService.list(new QueryWrapper<SeckillSkuRelationEntity>().eq("promotion_session_id",sessionId));
                sessionEntity.setRelationSkus(list);
                return sessionEntity;
            }).collect(Collectors.toList());
            return sessionEntities;
        }
        return null;
    }

    //获取起始时间
    private String starTime(){
        LocalDate now = LocalDate.now();//当前日期
        LocalTime min = LocalTime.MIN;//最小时间
        LocalDateTime startTime = LocalDateTime.of(now,min);
        String startTimeStr = startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return startTimeStr;
    }

    //获取结束时间
    private String endTime(){
        LocalDate now = LocalDate.now().plusDays(2);//当前日期-2
        LocalTime min = LocalTime.MAX;//最大时间
        LocalDateTime endTime = LocalDateTime.of(now,min);
        String endTimeStr = endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return endTimeStr;
    }

}