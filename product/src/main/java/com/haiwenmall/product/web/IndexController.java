package com.haiwenmall.product.web;

import com.haiwenmall.product.entity.CategoryEntity;
import com.haiwenmall.product.service.CategoryService;
import com.haiwenmall.product.vo.Catelog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping({"/","/index.html"})
    public String indexPage(Model model){

        //获取一级分类数据
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        //给视图加入数据
        model.addAttribute("categories",categoryEntities);
        /**
         * 视图解析器进行拼接
         * /classpath:/templates/ + 返回值 + .html
         */
        return "index";
    }

    @ResponseBody
    @GetMapping("/index/json/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJson(){
        Map<String, List<Catelog2Vo>> map = categoryService.getCatalogJson();
        return map;
    }
}
