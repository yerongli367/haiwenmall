package com.haiwenmall.product.app;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmail.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.haiwenmall.product.entity.SkuInfoEntity;
import com.haiwenmall.product.service.SkuInfoService;

/**
 * sku信息
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2022-03-17 15:40:28
 */
@RestController
@RequestMapping("product/skuinfo")
public class SkuInfoController {
    @Autowired
    private SkuInfoService skuInfoService;

    @GetMapping("{skuId}/price")
    public R getSkuPrice(@PathVariable("skuId")Long skuId){
        SkuInfoEntity skuEntity  = skuInfoService.getById(skuId);
        BigDecimal price = skuEntity.getPrice();
        return R.ok().setData(price.toString());
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = skuInfoService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId){
		SkuInfoEntity skuInfo = skuInfoService.getById(skuId);

        return R.ok().put("skuInfo", skuInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.save(skuInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.updateById(skuInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] skuIds){
		skuInfoService.removeByIds(Arrays.asList(skuIds));

        return R.ok();
    }

}
