package com.haiwenmall.product.feign;

import com.haiwenmail.common.utils.R;
import com.haiwenmall.product.fallback.SeckillFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * fallback：远程调用失败时调用子类重写的兜底方法
 */
@FeignClient(value = "seckill",fallback = SeckillFeignServiceFallBack.class)
public interface SeckillFeignService {

    /**
     * 查询当前sku是否参与秒杀优惠
     */
    @GetMapping("/sku/seckill/{skuId}")
    R getSkuSeckillInfo(@PathVariable("skuId")Long skuId);

}
