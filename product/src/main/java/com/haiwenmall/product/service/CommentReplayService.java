package com.haiwenmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2022-03-17 15:40:28
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

