package com.haiwenmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.product.entity.AttrEntity;
import com.haiwenmall.product.vo.AttrRespVo;
import com.haiwenmall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-07 20:37:22
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBaseAttrPage(String type,Map<String, Object> params, Long catelogId);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    PageUtils getNoRelationAttr(Long attrgroupId, Map<String, Object> params);

}

