package com.haiwenmall.product.fallback;

import com.haiwenmail.common.exception.BizCodeEnum;
import com.haiwenmail.common.utils.R;
import com.haiwenmall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 远程调用失败时的兜底方法
 */
@Slf4j
@Component
public class SeckillFeignServiceFallBack implements SeckillFeignService {

    @Override
    public R getSkuSeckillInfo(Long skuId) {
        log.info("熔断方法调用。。。SeckillFeignServiceFallBack的getSkuSeckillInfo()");
        return R.error(BizCodeEnum.TO_MANY_REQUEST.getCode(),BizCodeEnum.TO_MANY_REQUEST.getMessage());
    }

}
