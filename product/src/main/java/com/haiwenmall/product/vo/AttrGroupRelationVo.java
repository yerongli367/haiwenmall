package com.haiwenmall.product.vo;

import lombok.Data;

@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
