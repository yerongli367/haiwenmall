package com.haiwenmall.product.dao;

import com.haiwenmall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-07 20:37:22
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {

}
