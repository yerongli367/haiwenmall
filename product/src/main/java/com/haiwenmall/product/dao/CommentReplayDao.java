package com.haiwenmall.product.dao;

import com.haiwenmall.product.entity.CommentReplayEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价回复关系
 * 
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2022-03-17 15:40:28
 */
@Mapper
public interface CommentReplayDao extends BaseMapper<CommentReplayEntity> {
	
}
