package com.haiwenmall.product.dao;

import com.haiwenmall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2022-03-17 15:40:28
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
