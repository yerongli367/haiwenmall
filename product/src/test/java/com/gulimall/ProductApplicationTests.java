package com.gulimall;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haiwenmall.product.entity.BrandEntity;
import com.haiwenmall.product.service.BrandService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProductApplicationTests {
    @Resource
    BrandService brandService;

    @Test
    void contextLoads() {
        brandService.list(new QueryWrapper<BrandEntity>().eq("bran_id", 1));
    }

}
