package com.haiwenmall.member.dao;

import com.haiwenmall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ??Ա
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:52:55
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {

}
