package com.haiwenmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.member.entity.MemberEntity;
import com.haiwenmall.member.exception.PhoneExistException;
import com.haiwenmall.member.exception.UsernameExistException;
import com.haiwenmall.member.vo.MemberLoginVo;
import com.haiwenmall.member.vo.MemberRegistVo;
import com.haiwenmall.member.vo.SocialUser;

import java.util.Map;

/**
 * ??Ա
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-12-31 21:52:55
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkMobileUnique(String phone) throws PhoneExistException;

    void checkUserNameUnique(String username) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(SocialUser socialUser) throws Exception;
}

