package com.haiwenmall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.haiwenmail.common.utils.HttpUtils;
import com.haiwenmall.member.dao.MemberLevelDao;
import com.haiwenmall.member.entity.MemberLevelEntity;
import com.haiwenmall.member.exception.PhoneExistException;
import com.haiwenmall.member.exception.UsernameExistException;
import com.haiwenmall.member.vo.MemberLoginVo;
import com.haiwenmall.member.vo.MemberRegistVo;
import com.haiwenmall.member.vo.SocialUser;
import com.haiwenmall.member.dao.MemberDao;
import com.haiwenmall.member.entity.MemberEntity;
import com.haiwenmall.member.service.MemberService;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmail.common.utils.Query;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    private MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberRegistVo vo) {
        MemberEntity memberEntity =  new MemberEntity();
        //设置默认等级
        MemberLevelEntity memberLevelEntity = memberLevelDao.getDefaultLevel();
        memberEntity.setLevelId(memberLevelEntity.getId());
        //检查手机号
        checkMobileUnique(vo.getPhone());
        memberEntity.setMobile(vo.getPhone());
        //检查用户名
        checkUserNameUnique(vo.getUserName());
        memberEntity.setUsername(vo.getUserName());
        //密码加密
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(vo.getPassword());
        memberEntity.setPassword(encode);
        //用户名
        memberEntity.setNickname(vo.getUserName());
        //TODO 其他默认信息
        //保存
        this.baseMapper.insert(memberEntity);

    }

    @Override
    public void checkMobileUnique(String phone) throws PhoneExistException {
        int count = count(new QueryWrapper<MemberEntity>().eq("mobile",phone));
        if (count > 0 ){
            throw new PhoneExistException();
        }
    }
    @Override
    public void checkUserNameUnique(String username) throws UsernameExistException {
        int count = count(new QueryWrapper<MemberEntity>().eq("username",username));
        if (count > 0 ){
            throw new UsernameExistException();
        }

    }

    /**
     * 普通登录
     */
    @Override
    public MemberEntity login(MemberLoginVo vo) {
        //明文密码
        String password = vo.getPassword();
        MemberEntity memberEntity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("username", vo.getLoginacct())
                .or().eq("mobile", vo.getLoginacct()));
        if(memberEntity == null){
            return null;
        }else{
            //获取到数据库存放的密文password
            String passwordDb = memberEntity.getPassword();
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            //密码匹配
            boolean matches = bCryptPasswordEncoder.matches(password, passwordDb);
            if (matches){
                return memberEntity;
            }else {
                return null;
            }
        }
    }

    /**
     * 社交登录
     */
    @Override
    public MemberEntity login(SocialUser socialUser) throws Exception {
        //注册和登录合并的逻辑
        String uid = socialUser.getUid();
        //当前社交用户是否第一次登录
        MemberEntity memberEntity = getOne(new QueryWrapper<MemberEntity>().eq("social_id", uid));
        if (memberEntity != null){
            //用户已经注册
            memberEntity.setAccessToken(socialUser.getAccess_token());
            memberEntity.setExpiresIn(socialUser.getExpires_in());
            updateById(memberEntity);
            return memberEntity;
        }else {
            //查询当前社交用户的社交账号信息（昵称、性别等）
            Map<String,String> query = new HashMap<>();
            query.put("access_token",socialUser.getAccess_token());
            query.put("uid",socialUser.getUid());
            HttpResponse response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", new HashMap<String, String>(), query);

            if (response.getStatusLine().getStatusCode() == 200) {
                //查询成功
                String json = EntityUtils.toString(response.getEntity());
                JSONObject jsonObject = JSON.parseObject(json);
                String name = jsonObject.getString("name");
                String gender = jsonObject.getString("gender");
                String profileImageUrl = jsonObject.getString("profile_image_url");

                memberEntity.setNickname(name);
                memberEntity.setGender("m".equals(gender)?1:0);
                memberEntity.setHeader(profileImageUrl);
                memberEntity.setCreateTime(new Date());
                memberEntity.setSocialUid(socialUser.getUid());
                memberEntity.setAccessToken(socialUser.getAccess_token());
                memberEntity.setExpiresIn(socialUser.getExpires_in());

                //把用户信息插入到数据库中
                baseMapper.insert(memberEntity);

            }
            return memberEntity;
        }
    }

}