package com.haiwenmall.member.web;

import com.haiwenmail.common.utils.R;
import com.haiwenmall.member.feign.OrderFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MemberWebController {
    @Autowired
    private OrderFeignService orderFeignService;

    @GetMapping("/memberOrder.html")
    public String memberOrderPage(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                                  Model model){
        //获取支付宝给我们传来的所有请求数据


        //查出当前用户登录的所有订单
        Map<String, Object> param = new HashMap<>();
        param.put("page",pageNum.toString());
        R r = orderFeignService.listWithItem(param);
        model.addAttribute("orders",r);
        return "orderList";
    }
}
