package com.gulimall.auth.feign;

import com.gulimall.auth.vo.SocialUser;
import com.gulimall.auth.vo.UserLoginVo;
import com.gulimall.auth.vo.UserRegistVo;
import com.haiwenmail.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("member")
public interface MemberFeignService {

    @PostMapping("member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    @PostMapping("member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("member/member/oauth/login")
    R oauthLogin(@RequestBody SocialUser socialUser) throws Exception ;
}
