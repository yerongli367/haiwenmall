package com.gulimall.auth.service.impl;

import com.gulimall.auth.feign.ThirdPartyFeignService;
import com.gulimall.auth.service.LoginService;
import com.haiwenmail.common.constant.AuthServerConstant;
import com.haiwenmail.common.exception.BizCodeEnum;
import com.haiwenmail.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private ThirdPartyFeignService thirdPartyFeignService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Override
    public R sendCode(String phone) {
        //TODO 接口防刷
        String redisCode = stringRedisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);
        if (!StringUtils.isEmpty(redisCode)){
            long l = Long.parseLong(redisCode.split("_")[0]);
            //60秒内不能再发
            if (System.currentTimeMillis() - l < 6000){
                return R.error(BizCodeEnum.SMS_CODE_EXCEPTION.getCode(),BizCodeEnum.SMS_CODE_EXCEPTION.getMessage());
            }
        }
        String smsCode = UUID.randomUUID().toString().substring(0, 5);
        redisCode = smsCode+"_"+System.currentTimeMillis();
        stringRedisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX+phone,smsCode,10, TimeUnit.MINUTES);
        thirdPartyFeignService.sendCode(phone,smsCode);
        return R.ok();

    }
}
