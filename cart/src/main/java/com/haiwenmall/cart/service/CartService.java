package com.haiwenmall.cart.service;

import com.haiwenmall.cart.vo.CartItemVo;
import com.haiwenmall.cart.vo.CartVo;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface CartService {
    /**
     * 将购物项添加到购物车
     */
    CartItemVo addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    /**
     * 获取某个购物项
     */
    CartItemVo getCartItem(Long skuId);

    /**
     * 获取购物车
     */
    CartVo getCart() throws ExecutionException, InterruptedException;

    /**
     * 清空购物车
     */
    void cleanCart(String cartKey);

    /**
     * 勾选购物项
     */
    void checkItem(Long skuId, Integer check);

    /**
     * 修改购物项数量
     */
    void countItem(Long skuId, Integer num);

    /**
     * 删除购物项
     */
    void deleteItem(Long skuId);

    /**
     * 获取购物车中所有的购物项
     */
    List<CartItemVo> getCurrentUserItems();
}
