package com.haiwenmall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.haiwenmall.cart.feign.ProductFeignService;
import com.haiwenmall.cart.interceptor.CartInterceptor;
import com.haiwenmall.cart.service.CartService;
import com.haiwenmall.cart.vo.CartItemVo;
import com.haiwenmall.cart.vo.CartVo;
import com.haiwenmall.cart.vo.SkuInfoVo;
import com.haiwenmail.common.constant.CartConstant;
import com.haiwenmail.common.to.UserInfoTo;
import com.haiwenmail.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ProductFeignService productFeignService;
    @Autowired
    private ThreadPoolExecutor threadPool;

    /**
     * 删除购物项
     */
    public void deleteItem(Long skuId){
        BoundHashOperations cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    /**
     * 获取登录购物车中所有选中的购物项
     */
    @Override
    public List<CartItemVo> getCurrentUserItems() {
        UserInfoTo userInfo = CartInterceptor.threadLocal.get();
        String cartKey = CartConstant.CART_PREFIX +userInfo.getUserId();
        if (StringUtils.isEmpty(cartKey)){
            return null;
        }else {
            List<CartItemVo> cartItems = getCartItems(cartKey).stream().filter(cartItem->
                    cartItem.getCheck()
            ).map((item)->{
                //获取最新的价格
                R r = productFeignService.getSkuPrice(item.getSkuId());
                String skuPrice = (String)r.get("data");
                item.setPrice(new BigDecimal(skuPrice));
                return item;
            }).collect(Collectors.toList());
            return cartItems;
        }
    }

    /**
     * 修改购物项数量
     */
    @Override
    public void countItem(Long skuId, Integer num){
        CartItemVo cartItem = getCartItem(skuId);
        cartItem.setCount(num);
        String json = JSON.toJSONString(cartItem);
        BoundHashOperations cartOps = getCartOps();
        cartOps.put(skuId,json);
    }

    /**
     * 添加商品到购物车
     */
    @Override
    public CartItemVo addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        BoundHashOperations<String,Object,Object> cartOps = getCartOps();
        String res = (String)cartOps.get(skuId.toString());
        if (StringUtils.isEmpty(res)){
            //购物车无此商品，添加新商品到购物车
            CartItemVo cartItemVo = new CartItemVo();
            //1.远程查询当前要添加的商品的信息
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(()->{
                R r = productFeignService.getSkuInfo(skuId);
                SkuInfoVo skuInfo = r.getData("skuInfo", new TypeReference<SkuInfoVo>(){});
                //数据赋值操作
                cartItemVo.setSkuId(skuInfo.getSkuId());
                cartItemVo.setTitle(skuInfo.getSkuTitle());
                cartItemVo.setImage(skuInfo.getSkuDefaultImg());
                cartItemVo.setPrice(skuInfo.getPrice());
                cartItemVo.setCount(num);
            },threadPool);
            //2、远程查询当前商品所以的销售属性组合信息
            CompletableFuture<Void> getSkuSaleValuesTask = CompletableFuture.runAsync(()->{
                List<String> stringList = productFeignService.getSkuSaleAttrValues(skuId);
                cartItemVo.setSkuAttrValues(stringList);
            },threadPool);
            //等待所有的异步任务全部完成
            CompletableFuture.allOf(getSkuInfoTask, getSkuSaleValuesTask).get();
            String json = JSON.toJSONString(cartItemVo);
            cartOps.put(skuId.toString(),json);
            return cartItemVo;
        }else {
            //购物车有此商品，修改数量
            CartItemVo cartItemVo = JSON.parseObject(res, CartItemVo.class);
            cartItemVo.setCount(cartItemVo.getCount()+num);
            String json = JSON.toJSONString(cartItemVo);
            cartOps.put(skuId.toString(),json);
            return cartItemVo;
        }
    }

    /**
     * 获取单个购物项
     */
    @Override
    public CartItemVo getCartItem(Long skuId) {
        BoundHashOperations cartOps = getCartOps();
        String res = (String)cartOps.get(skuId.toString());
        CartItemVo cartItemVo = JSON.parseObject(res, CartItemVo.class);
        return cartItemVo;
    }

    /**
     * 获取购物车
     */
    @Override
    public CartVo getCart() throws ExecutionException, InterruptedException {
        UserInfoTo userInfo = CartInterceptor.threadLocal.get();
        CartVo cart = new CartVo();
        if (userInfo.getUserId() != null){
            //登录了,先判断临沭购物车是否有数据
            String tempCartKey = CartConstant.CART_PREFIX+userInfo.getUserKey();
            List<CartItemVo> cartItems = getCartItems(tempCartKey);
            if(!CollectionUtils.isEmpty(cartItems)){
                //临时购物车有数据.需要合并
                for (CartItemVo cartItem:cartItems ) {
                    addToCart(cartItem.getSkuId(),cartItem.getCount());
                }
            }
            String userCartKey = CartConstant.CART_PREFIX+userInfo.getUserId().toString();
            cart.setItems(getCartItems(userCartKey));
            //删除临时购物车
            cleanCart(tempCartKey);
        }else{
            //没登录
            String cartKey = CartConstant.CART_PREFIX+userInfo.getUserKey();
            List<CartItemVo> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        }

        return cart;
    }

    /**
     * 清空购物车
     */
    @Override
    public void cleanCart(String cartKey){
        redisTemplate.delete(cartKey);
    }

    /**
     * 勾选购物项
     */
    @Override
    public void checkItem(Long skuId, Integer check) {
        CartItemVo cartItem = getCartItem(skuId);
        cartItem.setCheck(check==1?true:false);
        String json = JSON.toJSONString(cartItem);
        BoundHashOperations cartOps = getCartOps();
        cartOps.put(skuId,json);
    }

    /**
     * 获取要操作的购物车redis
     * @return
     */
    private BoundHashOperations getCartOps(){
        UserInfoTo userInfo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (userInfo.getUserId() != null){
            cartKey = CartConstant.CART_PREFIX +userInfo.getUserId();
        }else {
            cartKey = CartConstant.CART_PREFIX+userInfo.getUserKey();
        }
        BoundHashOperations<String,Object,Object> cartOps = redisTemplate.boundHashOps(cartKey);
        return cartOps;
    }

    /**
     * 获取购物车的数据
     */
    private List<CartItemVo> getCartItems(String cartKey){
        BoundHashOperations<String,Object,Object> cartOps = redisTemplate.boundHashOps(cartKey);
        List<Object> values = cartOps.values();
        List<CartItemVo> cartItems = values.stream().map((value) -> {
            CartItemVo cartItemVo = JSON.parseObject(value.toString(), CartItemVo.class);
            return cartItemVo;
        }).collect(Collectors.toList());
        return cartItems;
    }


}
