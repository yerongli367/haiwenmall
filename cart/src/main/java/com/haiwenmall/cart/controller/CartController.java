package com.haiwenmall.cart.controller;

import com.haiwenmall.cart.service.CartService;
import com.haiwenmall.cart.vo.CartItemVo;
import com.haiwenmall.cart.vo.CartVo;
import com.haiwenmail.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 获取购物车中所有的购物项
     */
    @ResponseBody
    @GetMapping("/CurrentUserItems")
    public List<CartItemVo> getCurrentUserItems(){
        List<CartItemVo> itemList = cartService.getCurrentUserItems();
        return itemList;
    }

    /**
     * 删除购物项
     */
    @GetMapping("/countItem")
    public String deleteItem(@RequestParam("skuId") Long skuId){
        cartService.deleteItem(skuId);
        return "redirec:http://cart.gulimall.com/cartList.html";
    }

    /**
     * 修改购物项数量
     */
    @GetMapping("/countItem")
    public R countItem(@RequestParam("skuId") Long skuId,@RequestParam("num") Integer num){
        cartService.countItem(skuId,num);
        return R.ok();
    }

    /**
     * 选中
     */
    @GetMapping("")
    public R checkItem(@RequestParam("skuId") Long skuId,@RequestParam("check") Integer check){
        cartService.checkItem(skuId,check);
        return R.ok();
    }

    /**
     * 临时用户：浏览器有一个叫user-key的cookie，用来标示用户的身份，一个月后过期
     */
    @GetMapping("/cart.html")
    public String CartListPage(Model model) throws ExecutionException, InterruptedException {
        CartVo cart = cartService.getCart();
        model.addAttribute("cart",cart);
        return "cartList";
    }

    /**
     * 添加商品到购物车
     */
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes ra) throws ExecutionException, InterruptedException {
        cartService.addToCart(skuId,num);
        ra.addAttribute("skuId",skuId);
        //通过重定向防止刷新成功页带来的重复添加
        return "redirec:http://cart.gulimall.com/addToCart.html";
    }

    /**
     * 添加购物车成功页
     */
    @GetMapping("/addToCart.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId,Model model){
        //重定向到成功页面,再次查询购物车数据即可
        CartItemVo cartItem = cartService.getCartItem(skuId);
        model.addAttribute("cartItem",cartItem);
        return "success";
    }
}
