package com.haiwenmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.order.entity.OrderReturnApplyEntity;

import java.util.Map;

/**
 * 订单退货申请
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-08 06:59:14
 */
public interface OrderReturnApplyService extends IService<OrderReturnApplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

