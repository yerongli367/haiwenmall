package com.haiwenmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haiwenmail.common.utils.PageUtils;
import com.haiwenmall.order.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-08 06:59:14
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

