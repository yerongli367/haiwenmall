package com.haiwenmall.order.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 订单确认页要用的数据
 */
public class OrderConfirmVo {

    /**
     * 收货地址列表
     */
    @Getter @Setter
    private List<MemberAddressVo> address;
    /**
     * 所有选中的购物项
     */
    @Getter @Setter
    private List<OrderItemVo> items;

    /**
     * 优惠券信息
     */
    @Getter @Setter
    private Integer integration;

    /**
     * 库存信息
     */
    @Getter @Setter
    Map<Long,Boolean> stocks;


    /**
     * 防重令牌
     */
    @Getter @Setter
    private String orderToken;

    /**
     * 订单总额
     */
    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if (items != null){
            for (OrderItemVo item:items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum = sum.add(multiply);
            }
        }
        return sum;
    }

    /**
     * 应付总额
     */
    public BigDecimal getPayPrice() {
        BigDecimal sum = new BigDecimal("0");
        if (items != null){
            for (OrderItemVo item:items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum = sum.add(multiply);
            }
        }
        return sum;
    }

}

