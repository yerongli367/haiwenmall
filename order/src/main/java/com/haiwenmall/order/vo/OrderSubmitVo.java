package com.haiwenmall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单提交的数据
 */
@Data
public class OrderSubmitVo {
    private Long addrId;//收获地址的id

    private Integer payType;//支付方式

    //无需提交购买的商品，去购物车再获取一遍
    //优惠发票

    private String orderToken;//防重令牌

    private BigDecimal payPrice;//应付价格 验价

    private String note;//备注信息

    //用户相关信息直接去session中取
}
