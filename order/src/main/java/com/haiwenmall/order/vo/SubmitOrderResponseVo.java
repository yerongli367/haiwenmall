package com.haiwenmall.order.vo;

import com.haiwenmall.order.entity.OrderEntity;
import lombok.Data;

/**
 * 下单返回数据
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code;


}
