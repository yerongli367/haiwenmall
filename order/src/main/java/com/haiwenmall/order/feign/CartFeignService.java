package com.haiwenmall.order.feign;

import com.haiwenmall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("cart")
public interface CartFeignService {
    @GetMapping("/CurrentUserItems")
    List<OrderItemVo> getCurrentUserItems();
}
