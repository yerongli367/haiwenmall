package com.haiwenmall.order.dao;

import com.haiwenmall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-08 06:59:14
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {

}
