package com.haiwenmall.order.dao;

import com.haiwenmall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 *
 * @author liuhaiwen
 * @email liuhaiwen@gmail.com
 * @date 2021-08-08 06:59:14
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {

}
