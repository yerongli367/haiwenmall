package com.haiwenmall.order.web;

import com.alipay.api.AlipayApiException;
import com.haiwenmall.order.component.AlipayTemplate;
import com.haiwenmall.order.service.OrderService;
import com.haiwenmall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PayWebController {

    @Autowired
    private AlipayTemplate alipayTemplate;
    @Autowired
    private OrderService orderService;


    /**
     * 用户下单:支付宝支付
     * 1、让支付页让浏览器展示
     * 2、支付成功以后，跳转到用户的订单列表页
     */
    @GetMapping(value = "payOrder",produces = "text/html")
    public String payOrder(@RequestParam("orderSn")String orderSn) throws AlipayApiException {
        PayVo payVo = orderService.getOrderPay(orderSn);
        //返回的是一个页面将此页面直接交给浏览器就好
        String pay = alipayTemplate.pay(payVo);
        return pay;
    }

}
