package com.haiwenmall.order.web;

import com.haiwenmall.order.service.OrderService;
import com.haiwenmall.order.vo.OrderConfirmVo;
import com.haiwenmall.order.vo.OrderSubmitVo;
import com.haiwenmall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

@Controller
public class OrderWebController {

    @Autowired
    private OrderService orderService;

    /**
     * 结算跳到确认页
     */
    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData",confirmVo);
        return "confirm";
    }

    /**
     * 下单：创建订单，验令牌，验价格，锁库存
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes){
        SubmitOrderResponseVo orderResponseVo = orderService.submitOrder(vo);
        if (orderResponseVo.getCode() == 0){
            //下单成功去支付选择页
            model.addAttribute("submitOrderResp",orderResponseVo);
            return "pay";
        }else {
            String msg = "下单失败;";
            switch (orderResponseVo.getCode()){
                case 1: msg += msg + "订单信息过期，请刷新再次提交"; break;
                case 2: msg += msg + "订单商品价格发送变化，请确认后再次提交"; break;
                case 3: msg += msg + "库存锁定失败，商品库存不足";
            }
            redirectAttributes.addFlashAttribute("msg", msg);
            return "rediect:http://order.gulimall.com/toTrade";
        }

    }

}
