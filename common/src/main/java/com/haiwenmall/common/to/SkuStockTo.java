package com.haiwenmail.common.to;

import lombok.Data;

@Data
public class SkuStockTo {
    private Long skuId;
    private Boolean hasStock;
}
