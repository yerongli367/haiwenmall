package com.haiwenmail.common.constant;

public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-kye";
    public static final Integer TEMP_USER_COOKIE_IIMEOUT = 60*60*24*30;
    public static final String CART_PREFIX = "gulimall:cart:";
}
