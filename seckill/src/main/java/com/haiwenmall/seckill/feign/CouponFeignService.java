package com.haiwenmall.seckill.feign;

import com.haiwenmail.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


@FeignClient("coupon")
public interface CouponFeignService {

    @GetMapping("/coupon/seckillsession/late3DaySession")
    R getLate3DaySession();

}
