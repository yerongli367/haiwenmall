package com.haiwenmall.seckill.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.haiwenmail.common.to.mq.SeckillOrderTo;
import com.haiwenmail.common.utils.R;
import com.haiwenmail.common.vo.MemberRespVo;
import com.haiwenmall.seckill.feign.CouponFeignService;
import com.haiwenmall.seckill.feign.ProductFeignService;
import com.haiwenmall.seckill.interceptor.LoginUserInterceptor;
import com.haiwenmall.seckill.service.SeckillService;
import com.haiwenmall.seckill.to.SeckillSkuRedisTo;
import com.haiwenmall.seckill.vo.SeckillSessionWithSkusVo;
import com.haiwenmall.seckill.vo.SkuInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    private CouponFeignService couponFeignService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ProductFeignService productFeignService;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    private final String SESSION_CACHE_PREFIX = "seckill:sessions:";
    private final String SKUKILL_CACHE_PREFIX = "seckill:skus";
    private final String SKU_STOCK_SEMAPHORE = "seckill:stock:";

    @Override
    public void uploadSeckillSkuLatest3Days() {
        //扫描最近三天需要参与秒杀的服务
        R r = couponFeignService.getLate3DaySession();
        if (r.getCode() == 0){
            //上架商品
            List<SeckillSessionWithSkusVo> sessionData = r.getData("data", new TypeReference<List<SeckillSessionWithSkusVo>>() {
            });
            //保存到redis
            //1.缓存活动信息
            saveSessionInfo(sessionData);
            //2.缓存活动关联的商品信息
            saveSessionSkuInfos(sessionData);
        }
    }

    /**
     * 返回当前时间可以参与的秒杀商品信息
     */
    @Override
    public List<SeckillSkuRedisTo> getCurrentSeckillSkus() {
        //1.确定当前时间属于那个秒杀场次
        long time = new Date().getTime();
        /**
         * 自定义受保护的资源，资源名：seckillSkus
         */
        try(Entry entry = SphU.entry("seckillSkus")){
            Set<String> keys = redisTemplate.keys(SESSION_CACHE_PREFIX + "*");
            for (String key:keys) {
                //替换
                String replace = key.replace(SESSION_CACHE_PREFIX, "");
                //截取
                String[] s = replace.split("_");

                Long startTime = Long.parseLong(s[0]);
                Long endTime = Long.parseLong(s[1]);

                if (time >= startTime && time <= endTime){
                    //2.获取这个秒杀场次对应的所有商品id  value = List<场次id_skuId>
                    List<String> range = redisTemplate.opsForList().range(key,0,-1);
                    //3.获取商品信息
                    BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
                    List<String> skuList = ops.multiGet(range);
                    if (!CollectionUtils.isEmpty(skuList)){
                        List<SeckillSkuRedisTo> seckillSkuRedisTos = skuList.stream().map(sku -> {
                            SeckillSkuRedisTo redisTo = JSON.parseObject(sku, SeckillSkuRedisTo.class);
                            return redisTo;
                        }).collect(Collectors.toList());
                        return seckillSkuRedisTos;
                    }
                }
            }
        }catch (BlockException e){
            log.error("资源被限流,原因：{}",e.getMessage());
        }
        return null;
    }

    private SeckillSkuRedisTo blockHandel(BlockException e){
        log.error("getSkuSeckillInfo()被限流了，原因：{}",e.getMessage());
        return null;
    }

    /**
     * 根据skuId查询商品是否参加秒杀活动
     */
    @SentinelResource(value = "getSkuSeckillInfo", blockHandler = "blockHandel")//自定义资源名，失败的回调
    @Override
    public SeckillSkuRedisTo getSkuSeckillInfo(Long skuId) {
        //1、找到所有需要秒杀的商品的key信息---seckill:skus
        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);

        //拿到所有的key
        Set<String> keys = hashOps.keys();
        if (keys != null && keys.size() > 0) {
            //4-45 正则表达式进行匹配
            String reg = "\\d-" + skuId;
            for (String key : keys) {
                //如果匹配上了
                if (Pattern.matches(reg,key)) {
                    //从Redis中取出数据来
                    String redisValue = hashOps.get(key);
                    //进行序列化
                    SeckillSkuRedisTo redisTo = JSON.parseObject(redisValue, SeckillSkuRedisTo.class);

                    //随机码
                    Long currentTime = System.currentTimeMillis();
                    Long startTime = redisTo.getStartTime();
                    Long endTime = redisTo.getEndTime();
                    //如果当前时间大于等于秒杀活动开始时间并且要小于活动结束时间
                    if (currentTime >= startTime && currentTime <= endTime) {
                        return redisTo;
                    }
                    redisTo.setRandomCode(null);
                    return redisTo;
                }
            }
        }
        return null;
    }

    /**
     * 秒杀
     */
    @Override
    public String kill(String killId, String key, Integer num) {

        //获取用户信息
        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();

        //获取秒杀的商品信息
        BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
        String json = ops.get(killId);
        if (StringUtils.isEmpty(json)){
            return null;
        }else{
            SeckillSkuRedisTo redisTo = JSON.parseObject(json, SeckillSkuRedisTo.class);
            //校验时间
            Long endTime = redisTo.getEndTime();
            Long startTime = redisTo.getStartTime();
            long now = new Date().getTime();
            //校验随机码
            String randomCode = redisTo.getRandomCode();
            //校验购买数量
            Integer seckillLimit = redisTo.getSeckillLimit();
            //校验幂等性
            String redisKey = memberRespVo.getId()+"_"+killId;
            Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(redisKey,
                    num.toString(),
                    endTime - startTime,
                    TimeUnit.MICROSECONDS);

            if (now >= startTime && now <= endTime &&
                    randomCode.equals(key) &&
                    num <= seckillLimit &&
                    ifAbsent){

                //获取分布式信号量也就是库存
                RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + randomCode);

                boolean b = semaphore.tryAcquire(num);//减num
                if (b){
                    //占库存成功
                    String orderSn = IdWorker.getTimeId();
                    SeckillOrderTo orderTo = new SeckillOrderTo();
                    orderTo.setOrderSn(orderSn);
                    orderTo.setMemberId(memberRespVo.getId());
                    orderTo.setNum(num);
                    orderTo.setPromotionSessionId(redisTo.getPromotionSessionId());
                    orderTo.setSkuId(redisTo.getSkuId());
                    orderTo.setSeckillPrice(redisTo.getSeckillPrice());
                    rabbitTemplate.convertAndSend("order-event-exchange","order.seckill.order",orderTo);
                    return orderSn;//返回订单号就算秒杀成功
                }
                return null;
            }else{
                return null;
            }
        }
    }

    /**
     * 缓存活动信息
     */
    private void saveSessionInfo(List<SeckillSessionWithSkusVo> sessionData){
        sessionData.stream().forEach(session -> {
            Long startTime = session.getStartTime().getTime();
            Long endTime = session.getEndTime().getTime();
            String key = SESSION_CACHE_PREFIX+startTime+"_"+endTime;
            Boolean hasKey = redisTemplate.hasKey(key);
            /**
             * 该场次已经存在则不缓存
             */
            if (!hasKey){
                List<String> SkuIdList = session.getRelationSkus().stream().map(item -> {
                    String sessionId = item.getPromotionSessionId().toString();
                    String skuId = item.getSkuId().toString();
                    //活动场次+skuId
                    return sessionId+"_"+skuId;
                }).collect(Collectors.toList());
                redisTemplate.opsForList().leftPushAll(key,SkuIdList);
            }
        });
    }

    /**
     * 缓存活动关联的商品信息
     */
    private void saveSessionSkuInfos(List<SeckillSessionWithSkusVo> sessionData){
        sessionData.stream().forEach(session -> {
            BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
            session.getRelationSkus().stream().forEach(sku -> {
                //随机码  seckill?sku=1&key=dasfqf
                String token = UUID.randomUUID().toString().replace("-", "");

                Long skuId = sku.getSkuId();
                /**
                 * 该场次的该商品已经存在则不缓存
                 */
                if (!ops.hasKey(sku.getPromotionSessionId().toString()+"_"+skuId.toString())){
                    //缓存商品
                    SeckillSkuRedisTo redisTo = new SeckillSkuRedisTo();

                    //1.sku的基本信息
                    R r = productFeignService.skuInfo(skuId);
                    if(r.getCode() == 0){
                        SkuInfoVo skuInfo = r.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                        });
                        redisTo.setSkuInfo(skuInfo);
                    }
                    //2.sku的秒杀信息
                    BeanUtils.copyProperties(sku,redisTo);
                    //3.设置当前商品的秒杀时间信息
                    redisTo.setStartTime(session.getStartTime().getTime());
                    redisTo.setEndTime(session.getEndTime().getTime());
                    //4.随机码
                    redisTo.setRandomCode(token);

                    String jsonString = JSON.toJSONString(redisTo);
                    ops.put(sku.getPromotionSessionId().toString()+"_"+skuId.toString(),jsonString);

                    //库存作为分布式的信号量 限流
                    RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + token);
                    semaphore.trySetPermits(sku.getSeckillCount());
                }
           });
        });
    }
}
