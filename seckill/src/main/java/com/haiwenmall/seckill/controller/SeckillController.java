package com.haiwenmall.seckill.controller;

import com.haiwenmail.common.utils.R;
import com.haiwenmall.seckill.service.SeckillService;
import com.haiwenmall.seckill.to.SeckillSkuRedisTo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Controller
public class SeckillController {

    @Autowired
    private SeckillService seckillService;

    /**
     * 返回当前时间可以参与的秒杀商品信息
     */
    @ResponseBody
    @GetMapping("/getCurrentSeckillSkus")
    public R getCurrentSeckillSkus(){
        log.info("getCurrentSeckillSkus请求正在执行");
        List<SeckillSkuRedisTo> tos =  seckillService.getCurrentSeckillSkus();
        return R.ok().setData(tos);
    }

    /**
     * 查询当前sku是否参与秒杀优惠
     */
    @ResponseBody
    @GetMapping("/sku/seckill/{skuId}")
    public R getSkuSeckillInfo(@PathVariable("skuId")Long skuId){
        SeckillSkuRedisTo redisTo = seckillService.getSkuSeckillInfo(skuId);
        return R.ok().setData(redisTo);
    }

    /**
     *
     */
    @GetMapping("/kill")
    public String seckill(@RequestParam("killId")String killId,
                     @RequestParam("key")String key,
                     @RequestParam("num")Integer num,
                          Model model){

        String orderSn = seckillService.kill(killId,key,num);
        model.addAttribute("orderSn",orderSn);
        return "success";
    }

}
